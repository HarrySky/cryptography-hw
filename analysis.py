#!/usr/bin/python3
import string
from fractions import gcd
from collections import Counter

letters = list(string.ascii_uppercase)
letters_number = len(letters)
message = "TUMEEDIGCMTTOAAMSCEPIXLPDOGBLLTBIZDLSIQHIOLLIOTPDHXANMYPWDTPZUIHELCBUYOYAALUNEEEMETTNTCZDPRYGUNRLBOUCYOGQOEEHNBFHPSBTPIPRFIDEYOGUATTVNBQDEOEMFRPAGRGSEOEMHEYMNQZLJBLBTETREIFIZNNTMSDEFAYEYTBNFHPDNVSECSBNNAETYMMNOBLBTETRFMXFTNGMDEDTEIFHPRGPQYOIFKAVPRNAAUYDEMMSZNGWDUYAJIKBJRRIXIKIAOFHLTJPMTTTZIWEDSRVEEQOEBTEXTBLADPPRVPSZNJPMTTTJQXLXAXMEEYSRNARZTUMDSEOQWMNOTUIFAWLBNFHPOGPQRDCNVZOEIPMFHTSGWAEGEAICUTTRJDAGEFWXDTEEUMYARRNQREOECZRLTUMDTSAAPQRZIPIXLJBHBBOTNGTQSDLLLUEERLQZGEOFBQMEHRWZCZMVVSTTDRIXLMYUQYSPLSBTUDWRKAUWDVUMGTNREUTSOHBOOYTEIPINTVWZANIEKGMDTNVOETNJPUCSAAIDMJAYTAFHHBAQMPMOMDSLRRJDAGESTQEDAGBAPDPRMPBPFBZQTSERVQMJMNSQSLMBDQIQTUMEOWDVMDSCENTXYLRRJDAGEGPQNEHVAEUCEYGUSYTGPQOFTPWYELNLWRTSEZEMNEEQMMCSWBCXDSAIMBRPFRZDEOTUIFAWLFBMNOAALRIRHGETAEWRPMVPHRZQTSEAQEANAFMUNHHVKTTSEVVFECAPBUOYOSUMNJIALUVTDHIXLJRNBUOYAYLQCTSVWZMLKVVSPCOPMESPSBVQPCOPMESAEEAALOIRZBRZDHKQSLNBCFCZMRQZTPNQMPBJNBWZEXOFBMRXIRAFRJTBIHOTDGPUSARBJXEXJHAFADCBZFEKDVLEIYCRBTEJCNVFUDUNTXYXAXMDEERRIFPSYFQOAWLLQYPZSFQNLPTUMKMLKRQFENOAWYINAYTKIXPBAEIMLRBTEJSUWATOEFMDTPRFBTEYSGIZDTNTIZDQITPFIYGVAQANHFWXDTEEAUNOIIQPULLYGDAEIBVMLNOHZEEZFNKFIZNNNFECAYTNENAHAQTSEPWETZFECZNTNTQESFRRBABPAGTQADTNATIRHNAFHPCBAFOQSGIKIYG"

def find_ngrams_occurences(message: str, bigrams: dict, trigrams: dict) -> None:
    """Bigrams and trigrams occurrence analysis"""
    for a in range(letters_number):
        for b in range(letters_number):
            bigram = letters[a] + letters[b]
            occurrences = message.count(bigram)
            if occurrences > 2:
                bigrams[bigram] = occurrences

            for c in range(letters_number):
                trigram = letters[a] + letters[b] + letters[c]
                occurrences = message.count(trigram)
                if occurrences > 2:
                    trigrams[trigram] = occurrences

def find_all_indices(message: str, substring: str):
    """Finding all indices of substring in message"""
    start = 0
    while True:
        start = message.find(substring, start)
        if start == -1: return
        yield start
        start += len(substring)

def calculate_indices_spaces_apart_numbers(ngrams_indices: list) -> dict:
    """Calculate how many spaces apart indexes are from another ones"""
    ngrams_spaces_apart = {}
    for key, value in ngrams_indices.items():
        distances = []
        for i in range(len(value)):
            for j in range(len(value)):
                if j <= i: continue
                distances.append(value[j] - value[i])

        ngrams_spaces_apart[key] = distances

    return ngrams_spaces_apart

def flatten_dict_to_list(dict_to_flatten: dict) -> list:
    """Make flat list out dict."""
    flat_list = []
    for spaces_apart_list in dict_to_flatten.values():
        for item in spaces_apart_list:
            flat_list.append(item)

    return flat_list

def calculate_spaces_apart_gcd(spaces_apart: list) -> list:
    """Calculate Greatest Common Divisor of spaces apart numbers"""
    spaces_apart_gcd = []
    for i in range(len(spaces_apart)):
        for j in range(i+1, len(spaces_apart)):
            spaces_apart_gcd.append(gcd(spaces_apart[j], spaces_apart[i]))

    return spaces_apart_gcd

def analyze_message(message: str) -> None:
    # Find bigrams and trigrams occurrences 
    bigrams = {}
    trigrams = {}
    find_ngrams_occurences(message, bigrams, trigrams)
    # Sort bigrams and trigrams occurrences by frequency 
    bi_sorted_by_frequency = [(key, bigrams[key]) for key in sorted(bigrams, key=bigrams.get, reverse=True)]
    tri_sorted_by_frequency = [(key, trigrams[key]) for key in sorted(trigrams, key=trigrams.get, reverse=True)]

    # Find indices for top 10 bigrams and trigrams
    bigrams_indices = {}
    trigrams_indices = {}
    for n in range(10):
        bigram = bi_sorted_by_frequency[n][0]
        bigrams_indices[bigram] = list(find_all_indices(message, bigram))
        trigram = tri_sorted_by_frequency[n][0]
        trigrams_indices[trigram] = list(find_all_indices(message, trigram))

    # Finding spaces apart between indices of bigrams and trigrams
    bi_spaces_apart = calculate_indices_spaces_apart_numbers(bigrams_indices)
    tri_spaces_apart = calculate_indices_spaces_apart_numbers(trigrams_indices)
    # Flatten the spaces apart dicts to list
    bi_all_spaces_apart = flatten_dict_to_list(bi_spaces_apart)
    tri_all_spaces_apart = flatten_dict_to_list(tri_spaces_apart)

    # Calculating GCD for all elements
    bi_spaces_apart_gcd = calculate_spaces_apart_gcd(bi_all_spaces_apart)
    tri_spaces_apart_gcd = calculate_spaces_apart_gcd(tri_all_spaces_apart)
    
    # Printing out most common GCDs
    print("> Most Common GCDs of Bigrams Spaces Apart Numbers:")
    print(Counter(bi_spaces_apart_gcd).most_common(10))
    print()
    print("> Most Common GCDs of Trigrams Spaces Apart Numbers:")
    print(Counter(tri_spaces_apart_gcd).most_common(10))
    print()
    print("-------------")

def get_nth_letters(message: str, n: int) -> str:
    """Return string with every Nth letter from message"""
    return message[::n]

analyze_message(message)
# User should decide what key length will be used
key_length = int(input("Enter key length that you think is right according to data analysis gathered: "))
# Default keyword is A times the key length (ex. AAAAAA for key length 6)
keyword = list("A" * key_length)
# Doubled alphabet letters are needed to calculate the offset when mapping letters 
double_letters = string.ascii_uppercase + string.ascii_uppercase
# Most common letters in English language (for analysis)
most_common_letters = "ETAOINSRHLDCU"
# List with replaced letters at different positions, which will be used to build decoded message
replaced_letters = []

for i in range(key_length):
    print("-------------")
    print()
    print("> All letters at position " + str(i+1) + ":")
    letters_at_position = get_nth_letters(message[i:], key_length)
    print(letters_at_position)
    print()
    print("> Top 10 Most Common Letters of Them: (Most Common Letters In English Alphabet - " + most_common_letters + ")")
    print(Counter(letters_at_position).most_common(10))
    print()

    while True:
        mapping_choice = str(input("Choose what letter you want to map to " + most_common_letters[0] + " (Hit enter to just map A to A, B to B, etc.): "))
        if len(mapping_choice) == 0: mapping_choice = most_common_letters[0]
        mapping_choice = mapping_choice[0].upper()
        if mapping_choice not in letters: continue
        print()

        print("Mapping " + mapping_choice + " to " + most_common_letters[0])
        most_common_letter_index = double_letters.find(most_common_letters[0])
        offset = double_letters[most_common_letter_index:].find(mapping_choice)
        print("Offset is " + str(offset) + " - Letter is " + letters[offset])
        print()

        letters_at_position_new = ""
        for n in range(len(letters_at_position)):
            letter_index = double_letters.find(letters_at_position[n], len(letters)-1)
            letters_at_position_new += double_letters[letter_index - offset]

        if offset != 0:
            print("> Top 10 Most Common Letters After Remaping: (Most Common Letters In English Alphabet - " + most_common_letters + ")")
            print(Counter(letters_at_position_new).most_common(10))
            print()

        if offset == 0 or str(input("Continue (compare most common!)? (n - try again/not n - continue) ")) != "n":
            keyword[i] = letters[offset]
            replaced_letters.append(letters_at_position_new)
            break

print("-------------")
print()
print("> Keyword is " + "".join(keyword))

decoded_message = []
for i in range(len(replaced_letters[0])):
    for l in range(key_length):
        if len(replaced_letters[l]) == i: break
        decoded_message.append(replaced_letters[l][i])

print()
print("> Decoded Message:")
print(''.join(decoded_message))
